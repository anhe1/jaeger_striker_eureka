﻿using Telerik.WinControls.Data;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Retencion.Forms {
    partial class ComprobanteGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            this.IdDirectorio = new Telerik.WinControls.UI.RadTextBox();
            this.ReceptorNuevo = new Telerik.WinControls.UI.RadButton();
            this.ReceptorActualizar = new Telerik.WinControls.UI.RadButton();
            this.NumRegistro = new Telerik.WinControls.UI.RadTextBox();
            this.ResidenciaFiscal = new Telerik.WinControls.UI.RadTextBox();
            this.FechaTimbre = new Telerik.WinControls.UI.RadDateTimePicker();
            this.CURP = new Telerik.WinControls.UI.RadTextBox();
            this.ReceptorRFC = new Telerik.WinControls.UI.RadTextBox();
            this.Receptor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.Serie = new Telerik.WinControls.UI.RadDropDownList();
            this.Documento = new Telerik.WinControls.UI.RadDropDownList();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.preparar_contribuyentes = new System.ComponentModel.BackgroundWorker();
            this.providerError = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.panel3 = new Telerik.WinControls.UI.RadPanel();
            this.gridImpuestos = new Telerik.WinControls.UI.RadGridView();
            this.TImpuesto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel2 = new Telerik.WinControls.UI.RadPanel();
            this.label15 = new Telerik.WinControls.UI.RadLabel();
            this.MesFinal = new Telerik.WinControls.UI.RadDropDownList();
            this.CveRetenc = new Telerik.WinControls.UI.RadDropDownList();
            this.MesInicial = new Telerik.WinControls.UI.RadDropDownList();
            this.label14 = new Telerik.WinControls.UI.RadLabel();
            this.label13 = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.label12 = new Telerik.WinControls.UI.RadLabel();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.panel1 = new Telerik.WinControls.UI.RadPanel();
            this.Version = new Telerik.WinControls.UI.RadTextBox();
            this.label24 = new Telerik.WinControls.UI.RadLabel();
            this.LugarExpedicion = new Telerik.WinControls.UI.RadTextBox();
            this.label21 = new Telerik.WinControls.UI.RadLabel();
            this.ReceptorDomicilioFiscal = new Telerik.WinControls.UI.RadTextBox();
            this.label20 = new Telerik.WinControls.UI.RadLabel();
            this.Extranjero = new Telerik.WinControls.UI.RadCheckBox();
            this.label16 = new Telerik.WinControls.UI.RadLabel();
            this.MontoTotalOperacion = new Telerik.WinControls.UI.RadSpinEditor();
            this.MontoTotalGravado = new Telerik.WinControls.UI.RadSpinEditor();
            this.label17 = new Telerik.WinControls.UI.RadLabel();
            this.MontoTotalExento = new Telerik.WinControls.UI.RadSpinEditor();
            this.label18 = new Telerik.WinControls.UI.RadLabel();
            this.MontoTotalRetencion = new Telerik.WinControls.UI.RadSpinEditor();
            this.label19 = new Telerik.WinControls.UI.RadLabel();
            this.tabControl1 = new Telerik.WinControls.UI.RadPageView();
            this.tabComprobante = new Telerik.WinControls.UI.RadPageViewPage();
            this.tabCFDIRelacionado = new Telerik.WinControls.UI.RadPageViewPage();
            this.ISRCorrespondiente = new Telerik.WinControls.UI.RadSpinEditor();
            this.label22 = new Telerik.WinControls.UI.RadLabel();
            this.UtilidadBimestral = new Telerik.WinControls.UI.RadSpinEditor();
            this.label23 = new Telerik.WinControls.UI.RadLabel();
            this.Totales = new Telerik.WinControls.UI.RadGroupBox();
            this.ComprobanteRelacionado = new Jaeger.UI.Retencion.Forms.ComprobanteRelacionadoControl();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorNuevo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorActualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegistro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaTimbre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel3)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MesFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CveRetenc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MesInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Version)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorDomicilioFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Extranjero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalGravado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalExento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalRetencion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabComprobante.SuspendLayout();
            this.tabCFDIRelacionado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ISRCorrespondiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilidadBimestral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Totales)).BeginInit();
            this.Totales.SuspendLayout();
            this.SuspendLayout();
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(1232, 57);
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.Size = new System.Drawing.Size(27, 20);
            this.IdDirectorio.TabIndex = 63;
            // 
            // ReceptorNuevo
            // 
            this.ReceptorNuevo.Image = global::Jaeger.UI.Retencion.Properties.Resources.add_16px;
            this.ReceptorNuevo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ReceptorNuevo.Location = new System.Drawing.Point(446, 37);
            this.ReceptorNuevo.Name = "ReceptorNuevo";
            this.ReceptorNuevo.Size = new System.Drawing.Size(26, 20);
            this.ReceptorNuevo.TabIndex = 62;
            // 
            // ReceptorActualizar
            // 
            this.ReceptorActualizar.Image = global::Jaeger.UI.Retencion.Properties.Resources.refresh_16px;
            this.ReceptorActualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ReceptorActualizar.Location = new System.Drawing.Point(421, 37);
            this.ReceptorActualizar.Name = "ReceptorActualizar";
            this.ReceptorActualizar.Size = new System.Drawing.Size(26, 20);
            this.ReceptorActualizar.TabIndex = 61;
            // 
            // NumRegistro
            // 
            this.NumRegistro.Location = new System.Drawing.Point(342, 63);
            this.NumRegistro.Name = "NumRegistro";
            this.NumRegistro.ReadOnly = true;
            this.NumRegistro.Size = new System.Drawing.Size(116, 20);
            this.NumRegistro.TabIndex = 60;
            // 
            // ResidenciaFiscal
            // 
            this.ResidenciaFiscal.Location = new System.Drawing.Point(108, 63);
            this.ResidenciaFiscal.Name = "ResidenciaFiscal";
            this.ResidenciaFiscal.ReadOnly = true;
            this.ResidenciaFiscal.Size = new System.Drawing.Size(100, 20);
            this.ResidenciaFiscal.TabIndex = 59;
            // 
            // FechaTimbre
            // 
            this.FechaTimbre.Location = new System.Drawing.Point(907, 11);
            this.FechaTimbre.Name = "FechaTimbre";
            this.FechaTimbre.Size = new System.Drawing.Size(200, 20);
            this.FechaTimbre.TabIndex = 58;
            this.FechaTimbre.TabStop = false;
            this.FechaTimbre.Text = "viernes, 1 de abril de 2022";
            this.FechaTimbre.Value = new System.DateTime(2022, 4, 1, 20, 57, 7, 657);
            // 
            // CURP
            // 
            this.CURP.Location = new System.Drawing.Point(679, 37);
            this.CURP.MaxLength = 20;
            this.CURP.Name = "CURP";
            this.CURP.ReadOnly = true;
            this.CURP.Size = new System.Drawing.Size(124, 20);
            this.CURP.TabIndex = 57;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.Location = new System.Drawing.Point(514, 37);
            this.ReceptorRFC.MaxLength = 14;
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.ReadOnly = true;
            this.ReceptorRFC.Size = new System.Drawing.Size(107, 20);
            this.ReceptorRFC.TabIndex = 56;
            // 
            // Receptor
            // 
            this.Receptor.AutoSizeDropDownHeight = true;
            this.Receptor.AutoSizeDropDownToBestFit = true;
            this.Receptor.DisplayMember = "Nombre";
            // 
            // Receptor.NestedRadGridView
            // 
            this.Receptor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Receptor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Receptor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Receptor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Receptor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Receptor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Receptor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "Id";
            gridViewTextBoxColumn8.HeaderText = "Id";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Id";
            gridViewTextBoxColumn9.FieldName = "Nombre";
            gridViewTextBoxColumn9.HeaderText = "Nombre";
            gridViewTextBoxColumn9.Name = "Nombre";
            gridViewTextBoxColumn9.Width = 220;
            gridViewTextBoxColumn10.FieldName = "RFC";
            gridViewTextBoxColumn10.HeaderText = "RFC";
            gridViewTextBoxColumn10.Name = "_drctr_rfc";
            gridViewTextBoxColumn11.FieldName = "ResidenciaFiscal";
            gridViewTextBoxColumn11.HeaderText = "Residencia Fiscal";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "_drctr_resfis";
            gridViewTextBoxColumn12.FieldName = "ClaveUsoCFDI";
            gridViewTextBoxColumn12.HeaderText = "Uso CFDI";
            gridViewTextBoxColumn12.Name = "ClaveUsoCFDI";
            this.Receptor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.Receptor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Receptor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Receptor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Receptor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.Receptor.EditorControl.Name = "NestedRadGridView";
            this.Receptor.EditorControl.ReadOnly = true;
            this.Receptor.EditorControl.ShowGroupPanel = false;
            this.Receptor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Receptor.EditorControl.TabIndex = 0;
            this.Receptor.Location = new System.Drawing.Point(75, 37);
            this.Receptor.Name = "Receptor";
            this.Receptor.NullText = "Denominación o Razon Social del Receptor";
            this.Receptor.Size = new System.Drawing.Size(340, 20);
            this.Receptor.TabIndex = 11;
            this.Receptor.TabStop = false;
            this.Receptor.ValueMember = "Id";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Location = new System.Drawing.Point(600, 11);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(201, 20);
            this.FechaEmision.TabIndex = 54;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "viernes, 1 de abril de 2022";
            this.FechaEmision.Value = new System.DateTime(2022, 4, 1, 20, 57, 7, 695);
            // 
            // Folio
            // 
            this.Folio.Location = new System.Drawing.Point(370, 11);
            this.Folio.Name = "Folio";
            this.Folio.Size = new System.Drawing.Size(124, 20);
            this.Folio.TabIndex = 53;
            // 
            // Serie
            // 
            this.Serie.Location = new System.Drawing.Point(225, 11);
            this.Serie.Name = "Serie";
            this.Serie.Size = new System.Drawing.Size(103, 20);
            this.Serie.TabIndex = 52;
            // 
            // Documento
            // 
            this.Documento.Location = new System.Drawing.Point(81, 11);
            this.Documento.Name = "Documento";
            this.Documento.Size = new System.Drawing.Size(98, 20);
            this.Documento.TabIndex = 51;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(213, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 18);
            this.label10.TabIndex = 50;
            this.label10.Text = "Núm. de Reg. Tributario:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(9, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 18);
            this.label9.TabIndex = 49;
            this.label9.Text = "Residencia Fiscal:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(807, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 18);
            this.label8.TabIndex = 48;
            this.label8.Text = "Fecha Certificación:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(633, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 18);
            this.label7.TabIndex = 47;
            this.label7.Text = "CURP:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(478, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 18);
            this.label6.TabIndex = 46;
            this.label6.Text = "RFC:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(10, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 18);
            this.label5.TabIndex = 45;
            this.label5.Text = "Receptor:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(500, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 18);
            this.label4.TabIndex = 44;
            this.label4.Text = "Fecha de Emisión:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(332, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 18);
            this.label3.TabIndex = 42;
            this.label3.Text = "Folio:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(185, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 18);
            this.label2.TabIndex = 43;
            this.label2.Text = "Serie:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 18);
            this.label1.TabIndex = 41;
            this.label1.Text = "Documento:";
            // 
            // preparar_contribuyentes
            // 
            this.preparar_contribuyentes.DoWork += new System.ComponentModel.DoWorkEventHandler(this.PrepararContribuyentes_DoWork);
            this.preparar_contribuyentes.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.PrepararContribuyentes_RunWorkerCompleted);
            // 
            // providerError
            // 
            this.providerError.ContainerControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.HeaderText = "Detalle de Retenciones:";
            this.groupBox1.Location = new System.Drawing.Point(0, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1148, 145);
            this.groupBox1.TabIndex = 64;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle de Retenciones:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gridImpuestos);
            this.panel3.Controls.Add(this.TImpuesto);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(485, 18);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(661, 125);
            this.panel3.TabIndex = 32;
            // 
            // gridImpuestos
            // 
            this.gridImpuestos.AutoGenerateHierarchy = true;
            this.gridImpuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridImpuestos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridImpuestos.MasterTemplate.AllowAddNewRow = false;
            this.gridImpuestos.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "Base";
            gridViewTextBoxColumn13.HeaderText = "BaseRet";
            gridViewTextBoxColumn13.Name = "BaseRet";
            gridViewTextBoxColumn13.Width = 85;
            gridViewComboBoxColumn3.FieldName = "TipoImpuesto";
            gridViewComboBoxColumn3.HeaderText = "TipoImpuesto";
            gridViewComboBoxColumn3.Name = "TipoImpuesto";
            gridViewComboBoxColumn3.Width = 85;
            gridViewTextBoxColumn14.FieldName = "MontoRetenido";
            gridViewTextBoxColumn14.HeaderText = "MontoRetenido";
            gridViewTextBoxColumn14.Name = "MontoRetenido";
            gridViewTextBoxColumn14.Width = 85;
            gridViewComboBoxColumn4.FieldName = "TipoPago";
            gridViewComboBoxColumn4.HeaderText = "TipoPago";
            gridViewComboBoxColumn4.Name = "TipoPago";
            gridViewComboBoxColumn4.Width = 110;
            this.gridImpuestos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn13,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn14,
            gridViewComboBoxColumn4});
            this.gridImpuestos.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.gridImpuestos.Name = "gridImpuestos";
            this.gridImpuestos.ShowGroupPanel = false;
            this.gridImpuestos.Size = new System.Drawing.Size(661, 95);
            this.gridImpuestos.TabIndex = 1;
            // 
            // TImpuesto
            // 
            this.TImpuesto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImpuesto.Etiqueta = "Impuestos";
            this.TImpuesto.Location = new System.Drawing.Point(0, 0);
            this.TImpuesto.Name = "TImpuesto";
            this.TImpuesto.ShowActualizar = false;
            this.TImpuesto.ShowAutorizar = false;
            this.TImpuesto.ShowCerrar = false;
            this.TImpuesto.ShowEditar = false;
            this.TImpuesto.ShowExportarExcel = false;
            this.TImpuesto.ShowFiltro = true;
            this.TImpuesto.ShowGuardar = false;
            this.TImpuesto.ShowHerramientas = false;
            this.TImpuesto.ShowImagen = false;
            this.TImpuesto.ShowImprimir = false;
            this.TImpuesto.ShowNuevo = true;
            this.TImpuesto.ShowRemover = true;
            this.TImpuesto.Size = new System.Drawing.Size(661, 30);
            this.TImpuesto.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.MesFinal);
            this.panel2.Controls.Add(this.CveRetenc);
            this.panel2.Controls.Add(this.MesInicial);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.Descripcion);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.Ejercicio);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(2, 18);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(483, 125);
            this.panel2.TabIndex = 31;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(6, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 18);
            this.label15.TabIndex = 16;
            this.label15.Text = "Retención:";
            // 
            // MesFinal
            // 
            this.MesFinal.Location = new System.Drawing.Point(233, 63);
            this.MesFinal.Name = "MesFinal";
            this.MesFinal.Size = new System.Drawing.Size(93, 20);
            this.MesFinal.TabIndex = 30;
            // 
            // CveRetenc
            // 
            this.CveRetenc.Location = new System.Drawing.Point(74, 9);
            this.CveRetenc.Name = "CveRetenc";
            this.CveRetenc.Size = new System.Drawing.Size(388, 20);
            this.CveRetenc.TabIndex = 17;
            // 
            // MesInicial
            // 
            this.MesInicial.Location = new System.Drawing.Point(74, 63);
            this.MesInicial.Name = "MesInicial";
            this.MesInicial.Size = new System.Drawing.Size(93, 20);
            this.MesInicial.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(6, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 18);
            this.label14.TabIndex = 18;
            this.label14.Text = "Descripción:";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(172, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 18);
            this.label13.TabIndex = 28;
            this.label13.Text = "Mes Final:";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(74, 36);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(388, 20);
            this.Descripcion.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(6, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 18);
            this.label12.TabIndex = 27;
            this.label12.Text = "Mes Inicial:";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(343, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 18);
            this.label11.TabIndex = 25;
            this.label11.Text = "Ejercicio:";
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(399, 63);
            this.Ejercicio.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.Ejercicio.Minimum = new decimal(new int[] {
            2001,
            0,
            0,
            0});
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.NullableValue = new decimal(new int[] {
            2001,
            0,
            0,
            0});
            this.Ejercicio.Size = new System.Drawing.Size(63, 20);
            this.Ejercicio.TabIndex = 26;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ejercicio.Value = new decimal(new int[] {
            2001,
            0,
            0,
            0});
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Version);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.LugarExpedicion);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.ReceptorDomicilioFiscal);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.Extranjero);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.ReceptorNuevo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.ReceptorActualizar);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.NumRegistro);
            this.panel1.Controls.Add(this.IdDirectorio);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.ResidenciaFiscal);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.FechaTimbre);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.CURP);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.ReceptorRFC);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.Receptor);
            this.panel1.Controls.Add(this.Documento);
            this.panel1.Controls.Add(this.FechaEmision);
            this.panel1.Controls.Add(this.Serie);
            this.panel1.Controls.Add(this.Folio);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1121, 91);
            this.panel1.TabIndex = 65;
            // 
            // Version
            // 
            this.Version.Location = new System.Drawing.Point(1001, 63);
            this.Version.Name = "Version";
            this.Version.ReadOnly = true;
            this.Version.Size = new System.Drawing.Size(106, 20);
            this.Version.TabIndex = 69;
            this.Version.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(904, 64);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 18);
            this.label24.TabIndex = 68;
            this.label24.Text = "Versión:";
            // 
            // LugarExpedicion
            // 
            this.LugarExpedicion.Location = new System.Drawing.Point(1001, 37);
            this.LugarExpedicion.Name = "LugarExpedicion";
            this.LugarExpedicion.ReadOnly = true;
            this.LugarExpedicion.Size = new System.Drawing.Size(106, 20);
            this.LugarExpedicion.TabIndex = 67;
            this.LugarExpedicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(904, 38);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(94, 18);
            this.label21.TabIndex = 66;
            this.label21.Text = "Lugar Expedición:";
            // 
            // ReceptorDomicilioFiscal
            // 
            this.ReceptorDomicilioFiscal.Location = new System.Drawing.Point(554, 63);
            this.ReceptorDomicilioFiscal.Name = "ReceptorDomicilioFiscal";
            this.ReceptorDomicilioFiscal.ReadOnly = true;
            this.ReceptorDomicilioFiscal.Size = new System.Drawing.Size(90, 20);
            this.ReceptorDomicilioFiscal.TabIndex = 65;
            this.ReceptorDomicilioFiscal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(466, 64);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 18);
            this.label20.TabIndex = 64;
            this.label20.Text = "Domicilio Fiscal:";
            // 
            // Extranjero
            // 
            this.Extranjero.Location = new System.Drawing.Point(809, 39);
            this.Extranjero.Name = "Extranjero";
            this.Extranjero.Size = new System.Drawing.Size(70, 18);
            this.Extranjero.TabIndex = 63;
            this.Extranjero.Text = "Extranjero";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(4, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 18);
            this.label16.TabIndex = 66;
            this.label16.Text = "Total de la operación*:";
            // 
            // MontoTotalOperacion
            // 
            this.MontoTotalOperacion.DecimalPlaces = 2;
            this.MontoTotalOperacion.Location = new System.Drawing.Point(126, 17);
            this.MontoTotalOperacion.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalOperacion.Name = "MontoTotalOperacion";
            this.MontoTotalOperacion.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalOperacion.TabIndex = 67;
            this.MontoTotalOperacion.TabStop = false;
            this.MontoTotalOperacion.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalOperacion.ThousandsSeparator = true;
            // 
            // MontoTotalGravado
            // 
            this.MontoTotalGravado.DecimalPlaces = 2;
            this.MontoTotalGravado.Location = new System.Drawing.Point(311, 17);
            this.MontoTotalGravado.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalGravado.Name = "MontoTotalGravado";
            this.MontoTotalGravado.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalGravado.TabIndex = 69;
            this.MontoTotalGravado.TabStop = false;
            this.MontoTotalGravado.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalGravado.ThousandsSeparator = true;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(223, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 18);
            this.label17.TabIndex = 68;
            this.label17.Text = "Total Gravado*:";
            // 
            // MontoTotalExento
            // 
            this.MontoTotalExento.DecimalPlaces = 2;
            this.MontoTotalExento.Location = new System.Drawing.Point(492, 17);
            this.MontoTotalExento.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalExento.Name = "MontoTotalExento";
            this.MontoTotalExento.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalExento.TabIndex = 71;
            this.MontoTotalExento.TabStop = false;
            this.MontoTotalExento.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalExento.ThousandsSeparator = true;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(406, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 18);
            this.label18.TabIndex = 70;
            this.label18.Text = "Total Excento*:";
            // 
            // MontoTotalRetencion
            // 
            this.MontoTotalRetencion.DecimalPlaces = 2;
            this.MontoTotalRetencion.Location = new System.Drawing.Point(683, 17);
            this.MontoTotalRetencion.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.MontoTotalRetencion.Name = "MontoTotalRetencion";
            this.MontoTotalRetencion.Size = new System.Drawing.Size(92, 20);
            this.MontoTotalRetencion.TabIndex = 73;
            this.MontoTotalRetencion.TabStop = false;
            this.MontoTotalRetencion.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.MontoTotalRetencion.ThousandsSeparator = true;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(592, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(86, 18);
            this.label19.TabIndex = 72;
            this.label19.Text = "Total Retenido*:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabComprobante);
            this.tabControl1.Controls.Add(this.tabCFDIRelacionado);
            this.tabControl1.DefaultPage = this.tabComprobante;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedPage = this.tabCFDIRelacionado;
            this.tabControl1.Size = new System.Drawing.Size(1148, 141);
            this.tabControl1.TabIndex = 74;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // tabComprobante
            // 
            this.tabComprobante.Controls.Add(this.panel1);
            this.tabComprobante.ItemSize = new System.Drawing.SizeF(81F, 24F);
            this.tabComprobante.Location = new System.Drawing.Point(10, 33);
            this.tabComprobante.Name = "tabComprobante";
            this.tabComprobante.Padding = new System.Windows.Forms.Padding(3);
            this.tabComprobante.Size = new System.Drawing.Size(1127, 97);
            this.tabComprobante.Text = "Comprobante";
            // 
            // tabCFDIRelacionado
            // 
            this.tabCFDIRelacionado.Controls.Add(this.ComprobanteRelacionado);
            this.tabCFDIRelacionado.ItemSize = new System.Drawing.SizeF(100F, 24F);
            this.tabCFDIRelacionado.Location = new System.Drawing.Point(10, 33);
            this.tabCFDIRelacionado.Name = "tabCFDIRelacionado";
            this.tabCFDIRelacionado.Padding = new System.Windows.Forms.Padding(3);
            this.tabCFDIRelacionado.Size = new System.Drawing.Size(1127, 97);
            this.tabCFDIRelacionado.Text = "CFDI Relacionado";
            // 
            // ISRCorrespondiente
            // 
            this.ISRCorrespondiente.DecimalPlaces = 2;
            this.ISRCorrespondiente.Location = new System.Drawing.Point(1028, 17);
            this.ISRCorrespondiente.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.ISRCorrespondiente.Name = "ISRCorrespondiente";
            this.ISRCorrespondiente.Size = new System.Drawing.Size(92, 20);
            this.ISRCorrespondiente.TabIndex = 78;
            this.ISRCorrespondiente.TabStop = false;
            this.ISRCorrespondiente.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.ISRCorrespondiente.ThousandsSeparator = true;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(989, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 18);
            this.label22.TabIndex = 77;
            this.label22.Text = "ISR:";
            // 
            // UtilidadBimestral
            // 
            this.UtilidadBimestral.DecimalPlaces = 2;
            this.UtilidadBimestral.Location = new System.Drawing.Point(879, 17);
            this.UtilidadBimestral.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.UtilidadBimestral.Name = "UtilidadBimestral";
            this.UtilidadBimestral.Size = new System.Drawing.Size(92, 20);
            this.UtilidadBimestral.TabIndex = 76;
            this.UtilidadBimestral.TabStop = false;
            this.UtilidadBimestral.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.UtilidadBimestral.ThousandsSeparator = true;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(779, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 18);
            this.label23.TabIndex = 75;
            this.label23.Text = "Utilidad Bimestral:";
            // 
            // Totales
            // 
            this.Totales.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Totales.Controls.Add(this.label16);
            this.Totales.Controls.Add(this.ISRCorrespondiente);
            this.Totales.Controls.Add(this.MontoTotalOperacion);
            this.Totales.Controls.Add(this.label22);
            this.Totales.Controls.Add(this.label17);
            this.Totales.Controls.Add(this.UtilidadBimestral);
            this.Totales.Controls.Add(this.MontoTotalGravado);
            this.Totales.Controls.Add(this.label23);
            this.Totales.Controls.Add(this.label18);
            this.Totales.Controls.Add(this.MontoTotalRetencion);
            this.Totales.Controls.Add(this.MontoTotalExento);
            this.Totales.Controls.Add(this.label19);
            this.Totales.Dock = System.Windows.Forms.DockStyle.Top;
            this.Totales.HeaderText = "Totales";
            this.Totales.Location = new System.Drawing.Point(0, 286);
            this.Totales.Name = "Totales";
            this.Totales.Size = new System.Drawing.Size(1148, 44);
            this.Totales.TabIndex = 79;
            this.Totales.TabStop = false;
            this.Totales.Text = "Totales";
            // 
            // ComprobanteRelacionado
            // 
            this.ComprobanteRelacionado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComprobanteRelacionado.Editable = true;
            this.ComprobanteRelacionado.Location = new System.Drawing.Point(3, 3);
            this.ComprobanteRelacionado.Name = "ComprobanteRelacionado";
            this.ComprobanteRelacionado.Size = new System.Drawing.Size(1121, 91);
            this.ComprobanteRelacionado.TabIndex = 0;
            // 
            // ComprobanteGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Totales);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Name = "ComprobanteGeneralControl";
            this.Size = new System.Drawing.Size(1148, 333);
            this.Load += new System.EventHandler(this.ComprobanteGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorNuevo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorActualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegistro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaTimbre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel3)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridImpuestos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MesFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CveRetenc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MesInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Version)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorDomicilioFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Extranjero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalGravado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalExento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoTotalRetencion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabComprobante.ResumeLayout(false);
            this.tabCFDIRelacionado.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ISRCorrespondiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilidadBimestral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Totales)).EndInit();
            this.Totales.ResumeLayout(false);
            this.Totales.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Telerik.WinControls.UI.RadTextBox IdDirectorio;
        private Telerik.WinControls.UI.RadLabel label10;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label8;
        private Telerik.WinControls.UI.RadLabel label7;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadLabel label5;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadLabel label1;
        private System.ComponentModel.BackgroundWorker preparar_contribuyentes;
        private System.Windows.Forms.ErrorProvider providerError;
        protected internal Telerik.WinControls.UI.RadTextBox NumRegistro;
        protected internal Telerik.WinControls.UI.RadTextBox ResidenciaFiscal;
        protected internal Telerik.WinControls.UI.RadDateTimePicker FechaTimbre;
        protected internal Telerik.WinControls.UI.RadTextBox CURP;
        protected internal Telerik.WinControls.UI.RadTextBox ReceptorRFC;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Receptor;
        protected internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        protected internal Telerik.WinControls.UI.RadTextBox Folio;
        protected internal Telerik.WinControls.UI.RadDropDownList Serie;
        protected internal Telerik.WinControls.UI.RadDropDownList Documento;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel label11;
        private Telerik.WinControls.UI.RadLabel label14;
        private Telerik.WinControls.UI.RadLabel label15;
        private Telerik.WinControls.UI.RadPanel panel1;
        protected internal Telerik.WinControls.UI.RadSpinEditor Ejercicio;
        protected internal Telerik.WinControls.UI.RadTextBox Descripcion;
        protected internal Telerik.WinControls.UI.RadDropDownList CveRetenc;
        private Telerik.WinControls.UI.RadPanel panel3;
        protected internal Common.Forms.ToolBarStandarControl TImpuesto;
        private Telerik.WinControls.UI.RadPanel panel2;
        private Telerik.WinControls.UI.RadLabel label13;
        private Telerik.WinControls.UI.RadLabel label12;
        protected internal Telerik.WinControls.UI.RadCheckBox Extranjero;
        private Telerik.WinControls.UI.RadLabel label19;
        private Telerik.WinControls.UI.RadLabel label18;
        private Telerik.WinControls.UI.RadLabel label17;
        private Telerik.WinControls.UI.RadLabel label16;
        protected internal Telerik.WinControls.UI.RadSpinEditor MontoTotalRetencion;
        protected internal Telerik.WinControls.UI.RadSpinEditor MontoTotalExento;
        protected internal Telerik.WinControls.UI.RadSpinEditor MontoTotalGravado;
        protected internal Telerik.WinControls.UI.RadSpinEditor MontoTotalOperacion;
        protected internal Telerik.WinControls.UI.RadDropDownList MesFinal;
        protected internal Telerik.WinControls.UI.RadDropDownList MesInicial;
        protected internal Telerik.WinControls.UI.RadTextBox ReceptorDomicilioFiscal;
        private Telerik.WinControls.UI.RadLabel label20;
        protected internal Telerik.WinControls.UI.RadTextBox LugarExpedicion;
        private Telerik.WinControls.UI.RadLabel label21;
        private Telerik.WinControls.UI.RadPageView tabControl1;
        private Telerik.WinControls.UI.RadPageViewPage tabComprobante;
        private Telerik.WinControls.UI.RadPageViewPage tabCFDIRelacionado;
        protected internal Telerik.WinControls.UI.RadSpinEditor ISRCorrespondiente;
        private Telerik.WinControls.UI.RadLabel label22;
        protected internal Telerik.WinControls.UI.RadSpinEditor UtilidadBimestral;
        private Telerik.WinControls.UI.RadLabel label23;
        private Telerik.WinControls.UI.RadGroupBox Totales;
        protected internal Telerik.WinControls.UI.RadTextBox Version;
        private Telerik.WinControls.UI.RadLabel label24;
        protected internal Telerik.WinControls.UI.RadGridView gridImpuestos;
        protected internal ComprobanteRelacionadoControl ComprobanteRelacionado;
        protected internal RadButton ReceptorNuevo;
        protected internal RadButton ReceptorActualizar;
    }
}
