﻿
namespace Jaeger.UI.Retencion.Forms {
    partial class ToolBarComprobanteFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolBarComprobanteFiscalControl));
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarComprobanteFiscal = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Emisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.Status = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Duplicar = new Telerik.WinControls.UI.CommandBarButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Certificar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.FilePDF = new Telerik.WinControls.UI.CommandBarButton();
            this.FileXML = new Telerik.WinControls.UI.CommandBarButton();
            this.Email = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelUuid = new Telerik.WinControls.UI.CommandBarLabel();
            this.IdDocumento = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.Complementos = new Telerik.WinControls.UI.CommandBarDropDownButton();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.MaximumSize = new System.Drawing.Size(0, 30);
            this.RadCommandBar1.MinimumSize = new System.Drawing.Size(0, 30);
            this.RadCommandBar1.Name = "RadCommandBar1";
            // 
            // 
            // 
            this.RadCommandBar1.RootElement.MaxSize = new System.Drawing.Size(0, 30);
            this.RadCommandBar1.RootElement.MinSize = new System.Drawing.Size(0, 30);
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(1319, 30);
            this.RadCommandBar1.TabIndex = 4;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarComprobanteFiscal});
            this.CommandBarRowElement1.Text = "";
            // 
            // ToolBarComprobanteFiscal
            // 
            this.ToolBarComprobanteFiscal.DisplayName = "Emision de Comprobante";
            this.ToolBarComprobanteFiscal.EnableFocusBorder = false;
            this.ToolBarComprobanteFiscal.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.Emisor,
            this.Separador,
            this.ToolLabelStatus,
            this.Status,
            this.Nuevo,
            this.Duplicar,
            this.Guardar,
            this.Actualizar,
            this.Certificar,
            this.Cancelar,
            this.FilePDF,
            this.FileXML,
            this.Email,
            this.Separator2,
            this.Complementos,
            this.CommandBarSeparator2,
            this.ToolLabelUuid,
            this.IdDocumento,
            this.Cerrar});
            this.ToolBarComprobanteFiscal.Name = "ToolBarComprobanteFiscal";
            // 
            // 
            // 
            this.ToolBarComprobanteFiscal.OverflowButton.Enabled = true;
            this.ToolBarComprobanteFiscal.ShowHorizontalLine = false;
            this.ToolBarComprobanteFiscal.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBarComprobanteFiscal.GetChildAt(2))).Enabled = true;
            // 
            // Emisor
            // 
            this.Emisor.DefaultItem = null;
            this.Emisor.DisplayName = "Emisor";
            this.Emisor.DrawText = true;
            this.Emisor.Image = global::Jaeger.UI.Retencion.Properties.Resources.administrator_male_16px;
            this.Emisor.Name = "Emisor";
            this.Emisor.Text = "XXXXXXXXXXXXXX";
            this.Emisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador 2";
            this.Separador.Name = "Separador";
            this.Separador.VisibleInOverflowMenu = false;
            // 
            // ToolLabelStatus
            // 
            this.ToolLabelStatus.DisplayName = "Etiqueta Status";
            this.ToolLabelStatus.Name = "ToolLabelStatus";
            this.ToolLabelStatus.Text = "Status:";
            // 
            // Status
            // 
            this.Status.AutoCompleteDisplayMember = "Descripcion";
            this.Status.AutoCompleteValueMember = "Id";
            this.Status.DisplayMember = "Descripcion";
            this.Status.DisplayName = "Descripcion";
            this.Status.DrawImage = false;
            this.Status.DrawText = true;
            this.Status.DropDownAnimationEnabled = true;
            this.Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Status.Image = ((System.Drawing.Image)(resources.GetObject("Status.Image")));
            this.Status.MaxDropDownItems = 0;
            this.Status.Name = "Status";
            this.Status.Text = "";
            this.Status.ValueMember = "Id";
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Retencion.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Duplicar
            // 
            this.Duplicar.DisplayName = "Duplicar";
            this.Duplicar.DrawText = true;
            this.Duplicar.Image = global::Jaeger.UI.Retencion.Properties.Resources.copy_16px;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Text = "Duplicar";
            this.Duplicar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Duplicar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Enabled = false;
            this.Guardar.Image = global::Jaeger.UI.Retencion.Properties.Resources.save_16px;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Retencion.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Certificar
            // 
            this.Certificar.DisplayName = "Timbrar";
            this.Certificar.DrawText = true;
            this.Certificar.Enabled = false;
            this.Certificar.Image = global::Jaeger.UI.Retencion.Properties.Resources.approval_16px;
            this.Certificar.Name = "Certificar";
            this.Certificar.Text = "Timbrar";
            this.Certificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Enabled = false;
            this.Cancelar.Image = global::Jaeger.UI.Retencion.Properties.Resources.cancel_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // FilePDF
            // 
            this.FilePDF.DisplayName = "Archivo PDF";
            this.FilePDF.DrawText = true;
            this.FilePDF.Enabled = false;
            this.FilePDF.Image = global::Jaeger.UI.Retencion.Properties.Resources.pdf_16px;
            this.FilePDF.Name = "FilePDF";
            this.FilePDF.Text = "PDF";
            this.FilePDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // FileXML
            // 
            this.FileXML.DisplayName = "Archivo XML";
            this.FileXML.DrawText = true;
            this.FileXML.Enabled = false;
            this.FileXML.Image = global::Jaeger.UI.Retencion.Properties.Resources.xml_file_16px;
            this.FileXML.Name = "FileXML";
            this.FileXML.Text = "XML";
            this.FileXML.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Email
            // 
            this.Email.DisplayName = "Correo";
            this.Email.DrawText = true;
            this.Email.Enabled = false;
            this.Email.Image = global::Jaeger.UI.Retencion.Properties.Resources.email_document_16px;
            this.Email.Name = "Email";
            this.Email.Text = "Envíar";
            this.Email.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolLabelUuid
            // 
            this.ToolLabelUuid.DisplayName = "UUID";
            this.ToolLabelUuid.Name = "ToolLabelUuid";
            this.ToolLabelUuid.Text = "UUID:";
            this.ToolLabelUuid.VisibleInOverflowMenu = false;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DisplayName = "IdDocumento";
            this.IdDocumento.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.IdDocumento.MinSize = new System.Drawing.Size(240, 22);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Text = "";
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Retencion.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Complementos
            // 
            this.Complementos.DisplayName = "Complementos";
            this.Complementos.Image = ((System.Drawing.Image)(resources.GetObject("Complementos.Image")));
            this.Complementos.Name = "Complementos";
            this.Complementos.Text = "Complementos";
            // 
            // ToolBarComprobanteFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RadCommandBar1);
            this.MinimumSize = new System.Drawing.Size(0, 30);
            this.Name = "ToolBarComprobanteFiscalControl";
            this.Size = new System.Drawing.Size(1319, 30);
            this.Load += new System.EventHandler(this.ToolBarComprobanteFiscalControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBarComprobanteFiscal;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelStatus;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelUuid;
        protected internal Telerik.WinControls.UI.CommandBarSplitButton Emisor;
        protected Telerik.WinControls.UI.CommandBarDropDownList Status;
        protected internal Telerik.WinControls.UI.CommandBarButton Nuevo;
        protected internal Telerik.WinControls.UI.CommandBarButton Duplicar;
        protected internal Telerik.WinControls.UI.CommandBarButton Guardar;
        protected internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        protected internal Telerik.WinControls.UI.CommandBarButton Certificar;
        protected internal Telerik.WinControls.UI.CommandBarButton Cancelar;
        protected internal Telerik.WinControls.UI.CommandBarButton FilePDF;
        protected internal Telerik.WinControls.UI.CommandBarButton FileXML;
        protected internal Telerik.WinControls.UI.CommandBarButton Email;
        protected internal Telerik.WinControls.UI.CommandBarTextBox IdDocumento;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Complementos;
    }
}
