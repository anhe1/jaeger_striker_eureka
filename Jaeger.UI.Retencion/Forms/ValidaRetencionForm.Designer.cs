﻿
namespace Jaeger.UI.Retencion.Forms {
    partial class ValidaRetencionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.title = new Telerik.WinControls.UI.RadLabel();
            this.TValida = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.EmisorRFC = new Telerik.WinControls.UI.RadTextBox();
            this.ReceptorRFC = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.IdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.Log = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmisorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Log)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.BackColor = System.Drawing.Color.White;
            this.title.Location = new System.Drawing.Point(11, 13);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(381, 18);
            this.title.TabIndex = 8;
            this.title.Text = "Verifica el folio fiscal de las facturas de retenciones e información de pagos";
            // 
            // TValida
            // 
            this.TValida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TValida.Etiqueta = "";
            this.TValida.Location = new System.Drawing.Point(0, 50);
            this.TValida.Name = "TValida";
            this.TValida.ReadOnly = false;
            this.TValida.ShowActualizar = true;
            this.TValida.ShowAutorizar = false;
            this.TValida.ShowCerrar = true;
            this.TValida.ShowEditar = false;
            this.TValida.ShowExportarExcel = false;
            this.TValida.ShowFiltro = false;
            this.TValida.ShowGuardar = false;
            this.TValida.ShowHerramientas = true;
            this.TValida.ShowImagen = false;
            this.TValida.ShowImprimir = false;
            this.TValida.ShowNuevo = true;
            this.TValida.ShowRemover = false;
            this.TValida.Size = new System.Drawing.Size(407, 30);
            this.TValida.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(407, 50);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Emisor RFC:";
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.EmisorRFC.Location = new System.Drawing.Point(96, 23);
            this.EmisorRFC.MaxLength = 14;
            this.EmisorRFC.Name = "EmisorRFC";
            this.EmisorRFC.Size = new System.Drawing.Size(101, 20);
            this.EmisorRFC.TabIndex = 10;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ReceptorRFC.Location = new System.Drawing.Point(290, 23);
            this.ReceptorRFC.MaxLength = 14;
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Size = new System.Drawing.Size(101, 20);
            this.ReceptorRFC.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(209, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Receptor RFC";
            // 
            // IdDocumento
            // 
            this.IdDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.IdDocumento.Location = new System.Drawing.Point(138, 51);
            this.IdDocumento.MaxLength = 36;
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Size = new System.Drawing.Size(254, 20);
            this.IdDocumento.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(15, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Id Documento (UUID):";
            // 
            // Log
            // 
            this.Log.AutoSize = false;
            this.Log.Location = new System.Drawing.Point(18, 93);
            this.Log.Multiline = true;
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(370, 149);
            this.Log.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(15, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 18);
            this.label4.TabIndex = 16;
            this.label4.Text = "Log:";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.EmisorRFC);
            this.groupBox1.Controls.Add(this.Log);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.IdDocumento);
            this.groupBox1.Controls.Add(this.ReceptorRFC);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(407, 249);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ValidaRetencionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 329);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.title);
            this.Controls.Add(this.TValida);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ValidaRetencionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Retención e Información de pagos";
            this.Load += new System.EventHandler(this.ValidaRetencionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmisorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Log)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel title;
        private Common.Forms.ToolBarStandarControl TValida;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadTextBox EmisorRFC;
        private Telerik.WinControls.UI.RadTextBox ReceptorRFC;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadTextBox IdDocumento;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadTextBox Log;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}