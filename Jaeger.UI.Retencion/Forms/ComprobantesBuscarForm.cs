﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.Aplication.Retencion.Contracts;
using Jaeger.Aplication.Retencion.Services;

namespace Jaeger.UI.Retencion.Forms {
    public partial class ComprobantesBuscarForm : RadForm {
        protected IComprobanteRetencionService service;
        protected Domain.Base.ValueObjects.CFDISubTipoEnum subTipo;
        private BindingList<RetencionSingle> datos;
        private string rfc;

        public void OnAgregar(RetencionSingle e) {
            if (this.AgregarComprobante != null)
                this.AgregarComprobante(this, e);
        }

        public event EventHandler<RetencionSingle> AgregarComprobante;

        public ComprobantesBuscarForm(string rfc, Domain.Base.ValueObjects.CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this.rfc = rfc;
            this.subTipo = subTipo;
        }

        private void ComprobantesBuscarForm_Load(object sender, EventArgs e) {
            this.ToolBarButtonActualizar.Text = "Buscar";
            this.service = new ComprobanteRetencionService();
        }

        private void ToolBarButtonAgregar_Click(object sender, EventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                var seleccionado = this.gridSearchResult.CurrentRow.DataBoundItem as RetencionSingle;
                if (seleccionado != null) {
                    this.OnAgregar(seleccionado);
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            if (this.workerBuscar.IsBusy == false) {
                this.radWaitingBar1.StartWaiting();
                this.workerBuscar.RunWorkerAsync();
            }
        }

        private void gridSearchResult_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                this.ToolBarButtonAgregar.PerformClick();
                this.Close();
            }
        }

        private void workerBuscar_DoWork(object sender, DoWorkEventArgs e) {
            this.datos = this.service.GetSearch(this.Folio.Text);
            e.Result = true;
        }

        private void workerBuscar_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            
        }

        private void workerBuscar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.gridSearchResult.DataSource = this.datos;
            this.radWaitingBar1.StopWaiting();
        }
    }
}
