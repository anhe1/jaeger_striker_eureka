﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.SAT.CRIP.Entities;

namespace Jaeger.UI.Retencion.Forms {
    public partial class ValidaRetencionForm : RadForm {
        private SAT.CRIP.Services.Interfaces.IService service = new SAT.CRIP.Services.ConsultaRetencionServiceWS();

        public ValidaRetencionForm() {
            InitializeComponent();
        }

        public ValidaRetencionForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ValidaRetencionForm_Load(object sender, EventArgs e) {
            this.TValida.Nuevo.Click += this.TValida_Nuevo_Click;
            this.TValida.Actualizar.Text = "Validar";
            this.TValida.Actualizar.Image = Properties.Resources.approval_16px;
            this.TValida.Actualizar.Click += this.TValida_Procesar_Click;
            this.TValida.Cerrar.Click += this.TValida_Cerrar_Click;
        }

        private void TValida_Nuevo_Click(object sender, EventArgs e) {
            this.EmisorRFC.Text = string.Empty;
            this.ReceptorRFC.Text = string.Empty;
            this.IdDocumento.Text = string.Empty;
            this.Log.Text = string.Empty;
        }

        private void TValida_Procesar_Click(object sender, EventArgs e) {
            if (this.ValidaForm() == false) {
                return;
            }
            var request = RetencionRequest.Create().WithEmisorRFC(this.EmisorRFC.Text).WithReceptorRFC(this.ReceptorRFC.Text).WithIdDocumento(this.IdDocumento.Text).Build();
            var response = this.service.Execute(request);
            this.Log.Text = response.ToString();
        }

        private void TValida_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private bool ValidaForm() {
            if (string.IsNullOrEmpty(this.EmisorRFC.Text)) {
                this.errorProvider1.SetError(this.EmisorRFC, "El RFC del emisor es requerido");
                return false;
            }

            if (string.IsNullOrEmpty(this.IdDocumento.Text)) {
                this.errorProvider1.SetError(this.EmisorRFC, "Id de documento no válido.");
                return false;
            }
            return true;
        }
    }
}
