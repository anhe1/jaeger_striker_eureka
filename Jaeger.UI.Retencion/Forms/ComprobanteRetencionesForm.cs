﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Retencion.Contracts;
using Jaeger.Aplication.Retencion.Services;
using Jaeger.Domain.Retencion.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Util.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;
using Jaeger.SAT.CRIP.Entities;

namespace Jaeger.UI.Retencion.Forms {
    public partial class ComprobanteRetencionesForm : RadForm {
        protected IComprobanteRetencionService service;
        protected SAT.CRIP.Services.Interfaces.IService serviceSAT;
        private BindingList<RetencionSingle> retenciones;
        protected internal RadMenuItem CopiarSeleccion = new RadMenuItem { Text = "Copiar" };
        protected internal RadMenuItem MenuContextualEstadoSAT = new RadMenuItem { Text = "Estado SAT" };
        protected internal RadMenuItem Adicional = new RadMenuItem { Text = "Acciones" };
        protected internal RadMenuItem Cargar = new RadMenuItem { Text = "Cargar" };
        protected internal RadMenuItem Descarga = new RadMenuItem { Text = "Descargar" };
        protected internal RadMenuItem SerializarCFDI = new RadMenuItem { Text = "Serializar XML" };

        public ComprobanteRetencionesForm() {
            InitializeComponent();
        }

        public ComprobanteRetencionesForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void RetencionesForm_Load(object sender, EventArgs e) {
            this.service = new ComprobanteRetencionService();
            this.serviceSAT = new SAT.CRIP.Services.ConsultaRetencionServiceWS();
            this.gridData.TelerikGridCommon();

            var _MesInicial = this.gridData.Columns["MesInicial"] as GridViewComboBoxColumn;
            _MesInicial.ValueMember = "Id";
            _MesInicial.DisplayMember = "Descripcion";
            _MesInicial.DataSource = ConfigService.GetMeses();

            var _MesFinal = this.gridData.Columns["MesFinal"] as GridViewComboBoxColumn;
            _MesFinal.ValueMember = "Id";
            _MesFinal.DisplayMember = "Descripcion";
            _MesFinal.DataSource = ConfigService.GetMeses();

            this.TRetencion.Nuevo.Click += this.TRetencion_Nuevo_Click;
            this.TRetencion.Editar.Click += this.TRetencion_Editar_Click;
            this.TRetencion.Actualizar.Click += this.TRetencion_Actualizar_Click;
            this.TRetencion.Filtro.Click += this.TRetencion_Filtro_Click;
            this.TRetencion.Cerrar.Click += this.TRetencion_Cerrar_Click;
            this.Adicional.Items.AddRange(new RadMenuItem[] { this.Cargar, this.Descarga, this.SerializarCFDI });
            this.menuContextual.Items.AddRange(new RadMenuItem[] { this.CopiarSeleccion, this.MenuContextualEstadoSAT, this.Adicional });
            this.CopiarSeleccion.Click += this.ContextMenuCopiar_Click;
            this.MenuContextualEstadoSAT.Click += this.ContextMenuEstadoSAT_Click;
        }

        #region barra de herramientas
        public virtual void TRetencion_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new ComprobanteRetencionForm(null, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) { MdiParent = this.ParentForm };
            _nuevo.Show();
        }

        public virtual void TRetencion_Editar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as RetencionSingle;
                if (_seleccionado != null) {
                    var _editar = new ComprobanteRetencionForm(new ComprobanteRetencionDetailModel { Id = _seleccionado.Id }, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) { MdiParent = this.ParentForm };
                    _editar.Show();
                }
            }
        }

        public virtual void TRetencion_Actualizar_Click(object sender, EventArgs e) {
            this.retenciones = this.service.GetList(this.TRetencion.GetEjercicio(), this.TRetencion.GetMes());
            this.gridData.DataSource = this.retenciones;
        }

        private void TRetencion_Filtro_Click(object sender, EventArgs e) {
            this.gridData.ActivateFilterRow(((CommandBarToggleButton)sender).ToggleState);
        }

        public virtual void TRetencion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region menu contextual
        public virtual void ContextMenuCopiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.gridData.CurrentCell.Value.ToString());
        }

        public virtual void ContextMenuEstadoSAT_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.EstadoSAT)) {
                espera.Text = "Consultando estado, un momento por favor...";
                espera.ShowDialog(this);
            }
            var response = (string)this.MenuContextualEstadoSAT.Tag;
            if (response != null)
                RadMessageBox.Show(this, response, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            this.MenuContextualEstadoSAT.Tag = null;
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.gridData.CurrentRow.ViewInfo.ViewTemplate == this.gridData.MasterTemplate) {
                    e.ContextMenu = this.menuContextual.DropDown;
                }
            }
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "FileXML" || e.Column.Name == "FilePDF") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    e.CellElement.Children.Clear();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.gridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name == "FileXML" || e.Column.Name == "FilePDF") {
                        var single = this.gridData.CurrentRow.DataBoundItem as RetencionSingle;
                        string liga = "";
                        if (e.Column.Name == "FileXML")
                            liga = single.FileXML;
                        else
                            if (e.Column.Name == "FilePDF")
                            liga = single.FilePDF;
                        // validar la liga de descarga
                        if (ValidacionService.URL(liga)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = Path.GetFileName(liga)
                            };
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            if (FileService.DownloadFile(liga, savefiledialog.FileName)) {
                                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (dr == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    } catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void EstadoSAT() {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as RetencionSingle;
                if (seleccionado != null) {
                    var request = RetencionRequest.Create().WithEmisorRFC(seleccionado.EmisorRFC).WithReceptorRFC(seleccionado.ReceptorRFC).WithIdDocumento(seleccionado.IdDocumento).Build();
                    var response = this.serviceSAT.Execute(request);
                    if (response.IsError == false) {
                        seleccionado.Estado = response.StatusComprobante;
                        seleccionado.FechaEstado = DateTime.Now;
                    }
                }
            }
        }
        #endregion
    }
}
