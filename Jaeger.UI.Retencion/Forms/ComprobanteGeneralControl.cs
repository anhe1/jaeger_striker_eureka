﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Retencion.Forms {
    public partial class ComprobanteGeneralControl : UserControl {
        protected BindingList<ComprobanteContribuyenteModel> contribuyenteModels;
        protected Aplication.Comprobante.Contracts.IContribuyenteService service;
        protected IRetencionesCatalogo retencionesCatalogo;
        protected IRetencionTipoImpuestoCatalogo catalogoTipoImpuesto;
        protected IRetencionTipoPagoCatalogo catalogoTipoPago;

        public ComprobanteGeneralControl() {
            InitializeComponent();
        }

        private void ComprobanteGeneralControl_Load(object sender, EventArgs e) {
            //this.Folio..TextBoxOnlyNumbers();
            this.gridImpuestos.TelerikGridCommon();
            this.MesInicial.ValueMember = "Id";
            this.MesInicial.DisplayMember = "Descripcion";
            this.MesInicial.DataSource = ConfigService.GetMeses();
            this.MesFinal.ValueMember = "Id";
            this.MesFinal.DisplayMember = "Descripcion";
            this.MesFinal.DataSource = ConfigService.GetMeses();
        }

        public virtual void Start() {
            this.retencionesCatalogo = new RetencionesCatalogo();
            this.retencionesCatalogo.Load();

            this.catalogoTipoImpuesto = new RetencionTipoImpuestoCatalogo();
            this.catalogoTipoPago = new RetencionTipoPagoCatalogo();

            this.CveRetenc.DisplayMember = "Descriptor";
            this.CveRetenc.ValueMember = "Clave";
            this.CveRetenc.DataSource = this.retencionesCatalogo.Items;

            var _TipoImpuesto = this.gridImpuestos.Columns["TipoImpuesto"] as GridViewComboBoxColumn;
            _TipoImpuesto.ValueMember = "Clave";
            _TipoImpuesto.DisplayMember = "Descriptor";
            _TipoImpuesto.DataSource = this.catalogoTipoImpuesto.Items;

            var _TipoPagoRet = this.gridImpuestos.Columns["TipoPago"] as GridViewComboBoxColumn;
            _TipoPagoRet.ValueMember = "Clave";
            _TipoPagoRet.DisplayMember = "Descriptor";
            _TipoPagoRet.DataSource = this.catalogoTipoPago.Items;

            this.ComprobanteRelacionado.Start();
        }

        public virtual void StartContribuyente(CFDISubTipoEnum subTipo) {
            this.service = new Aplication.Comprobante.Services.ContribuyenteService(subTipo);
            this.Receptor.Enabled = false;
            this.preparar_contribuyentes.RunWorkerAsync();
        }

        private void PrepararContribuyentes_DoWork(object sender, DoWorkEventArgs e) {
            this.ReceptorActualizar.Enabled = false;
            this.Receptor.Enabled = false;
            this.contribuyenteModels = this.service.GetContribuyentes();

        }

        private void PrepararContribuyentes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Receptor.SelectedIndex = -1;
            this.Receptor.SelectedValue = null;
            this.Receptor.Enabled = true;
            this.ReceptorActualizar.Enabled = true;
            this.Receptor.Enabled = true;
            this.Receptor.SelectedValueChanged += new EventHandler(this.Receptor_SelectedValueChanged);
            this.Receptor.DropDownOpened += new EventHandler(this.Receptor_DropDownOpened);
        }

        private void Receptor_DropDownOpened(object sender, EventArgs e) {
            this.Receptor.DataSource = this.contribuyenteModels;
        }

        private void Receptor_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.Receptor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var _seleccionado = temporal.DataBoundItem as ComprobanteContribuyenteModel;
                if (_seleccionado != null) {
                    this.ReceptorRFC.Text = _seleccionado.RFC;
                    this.IdDirectorio.Text = _seleccionado.IdDirectorio.ToString();

                    if (_seleccionado.CURP != null) {
                        this.CURP.Text = _seleccionado.CURP;
                    }

                    if (_seleccionado.DomicilioFiscal != null) {
                        this.ReceptorDomicilioFiscal.Text = _seleccionado.DomicilioFiscal;
                    }

                    if (_seleccionado.Extranjero) {
                        this.Extranjero.Checked = _seleccionado.Extranjero;
                    }
                } else {
                    MessageBox.Show(this, Properties.Resources.Message_Objeto_NoValido, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
