﻿namespace Jaeger.UI.Retencion.Forms {
    partial class ComprobanteFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new Telerik.WinControls.UI.RadPanel();
            this.General = new Jaeger.UI.Retencion.Forms.ComprobanteGeneralControl();
            this.TRetencion = new Jaeger.UI.Retencion.Forms.ToolBarComprobanteFiscalControl();
            this.OnStart = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.General);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1278, 336);
            this.groupBox1.TabIndex = 1;
            // 
            // General
            // 
            this.General.Dock = System.Windows.Forms.DockStyle.Fill;
            this.General.Location = new System.Drawing.Point(0, 0);
            this.General.Name = "General";
            this.General.Size = new System.Drawing.Size(1278, 336);
            this.General.TabIndex = 0;
            // 
            // TRetencion
            // 
            this.TRetencion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRetencion.Location = new System.Drawing.Point(0, 0);
            this.TRetencion.MinimumSize = new System.Drawing.Size(0, 30);
            this.TRetencion.Name = "TRetencion";
            this.TRetencion.Size = new System.Drawing.Size(1278, 30);
            this.TRetencion.TabIndex = 0;
            // 
            // OnStart
            // 
            this.OnStart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OnStart_DoWork);
            this.OnStart.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnStart_RunWorkerCompleted);
            // 
            // ComprobanteFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TRetencion);
            this.Name = "ComprobanteFiscalControl";
            this.Size = new System.Drawing.Size(1278, 366);
            this.Load += new System.EventHandler(this.ComprobanteFiscalControl_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel groupBox1;
        protected internal ToolBarComprobanteFiscalControl TRetencion;
        protected internal ComprobanteGeneralControl General;
        private System.ComponentModel.BackgroundWorker OnStart;
    }
}
