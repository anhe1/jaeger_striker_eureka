﻿namespace Jaeger.UI.Retencion.Forms {
    partial class ComprobanteRelacionadoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.checkBoxIncluir = new Telerik.WinControls.UI.RadCheckBox();
            this.CommandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostItemIncluir = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.TipoRelacion = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            this.ReceptorRFC = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Comprobantes = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).BeginInit();
            this.RadCommandBar2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxIncluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar2
            // 
            this.RadCommandBar2.Controls.Add(this.checkBoxIncluir);
            this.RadCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar2.Name = "RadCommandBar2";
            this.RadCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement2});
            this.RadCommandBar2.Size = new System.Drawing.Size(969, 55);
            this.RadCommandBar2.TabIndex = 1;
            // 
            // checkBoxIncluir
            // 
            this.checkBoxIncluir.Location = new System.Drawing.Point(20, 6);
            this.checkBoxIncluir.Name = "checkBoxIncluir";
            this.checkBoxIncluir.Size = new System.Drawing.Size(51, 18);
            this.checkBoxIncluir.TabIndex = 189;
            this.checkBoxIncluir.Text = "Incluir";
            this.checkBoxIncluir.CheckStateChanged += new System.EventHandler(this.ChkCfdiRelacionadoIncluir_CheckStateChanged);
            // 
            // CommandBarRowElement2
            // 
            this.CommandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement2.Name = "CommandBarRowElement2";
            this.CommandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.CommandBarRowElement2.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "CommandBarStripElement2";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarHostItemIncluir,
            this.commandBarSeparator3,
            this.CommandBarLabel1,
            this.TipoRelacion,
            this.CommandBarSeparator1,
            this.Agregar,
            this.Remover,
            this.ReceptorRFC});
            this.ToolBar.Name = "ToolBar";
            // 
            // 
            // 
            this.ToolBar.OverflowButton.Enabled = false;
            this.ToolBar.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBar.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarHostItemIncluir
            // 
            this.ToolBarHostItemIncluir.DisplayName = "Incluir";
            this.ToolBarHostItemIncluir.MinSize = new System.Drawing.Size(65, 18);
            this.ToolBarHostItemIncluir.Name = "ToolBarHostItemIncluir";
            this.ToolBarHostItemIncluir.Text = "Incluir";
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // CommandBarLabel1
            // 
            this.CommandBarLabel1.DisplayName = "CommandBarLabel1";
            this.CommandBarLabel1.Name = "CommandBarLabel1";
            this.CommandBarLabel1.Text = "Tipo de Relación: ";
            // 
            // TipoRelacion
            // 
            this.TipoRelacion.AutoSize = true;
            this.TipoRelacion.DisplayName = "Tipo de Relación";
            this.TipoRelacion.DropDownAnimationEnabled = true;
            this.TipoRelacion.Enabled = false;
            this.TipoRelacion.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.TipoRelacion.MaxDropDownItems = 0;
            this.TipoRelacion.MinSize = new System.Drawing.Size(350, 22);
            this.TipoRelacion.Name = "TipoRelacion";
            this.TipoRelacion.Padding = new System.Windows.Forms.Padding(0);
            this.TipoRelacion.Text = "";
            this.TipoRelacion.EnabledChanged += new System.EventHandler(this.TipoRelacion_EnabledChanged);
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "CommandBarSeparator1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // Agregar
            // 
            this.Agregar.DisplayName = "Agregar";
            this.Agregar.DrawText = true;
            this.Agregar.Enabled = false;
            this.Agregar.Image = global::Jaeger.UI.Retencion.Properties.Resources.add_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Agregar.Click += new System.EventHandler(this.TRelacion_Agregar_Click);
            // 
            // Remover
            // 
            this.Remover.DisplayName = "Quitar";
            this.Remover.DrawText = true;
            this.Remover.Enabled = false;
            this.Remover.Image = global::Jaeger.UI.Retencion.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Remover.Click += new System.EventHandler(this.TRelacion_Remover_Click);
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DisplayName = "Receptor RFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Text = "";
            this.ReceptorRFC.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // Comprobantes
            // 
            this.Comprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Comprobantes.Location = new System.Drawing.Point(0, 55);
            // 
            // 
            // 
            this.Comprobantes.MasterTemplate.AllowAddNewRow = false;
            this.Comprobantes.MasterTemplate.AllowEditRow = false;
            this.Comprobantes.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "IdDocumento";
            gridViewTextBoxColumn1.HeaderText = "UUID";
            gridViewTextBoxColumn1.Name = "IdDocumento";
            gridViewTextBoxColumn1.Width = 220;
            gridViewTextBoxColumn2.FieldName = "RFC";
            gridViewTextBoxColumn2.HeaderText = "RFC";
            gridViewTextBoxColumn2.Name = "RFC";
            gridViewTextBoxColumn2.Width = 110;
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 250;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Folio";
            gridViewTextBoxColumn5.HeaderText = "Folio";
            gridViewTextBoxColumn5.Name = "Folio";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "Total";
            gridViewTextBoxColumn6.FormatString = "{0:n}";
            gridViewTextBoxColumn6.HeaderText = "Total";
            gridViewTextBoxColumn6.Name = "Total";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 95;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "ImporteAplicado";
            gridViewTextBoxColumn7.FormatString = "{0:n}";
            gridViewTextBoxColumn7.HeaderText = "Importe";
            gridViewTextBoxColumn7.Name = "ImporteAplicado";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 95;
            this.Comprobantes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.Comprobantes.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Comprobantes.Name = "Comprobantes";
            this.Comprobantes.ShowGroupPanel = false;
            this.Comprobantes.Size = new System.Drawing.Size(969, 121);
            this.Comprobantes.TabIndex = 188;
            // 
            // ComprobanteRelacionadoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Comprobantes);
            this.Controls.Add(this.RadCommandBar2);
            this.Name = "ComprobanteRelacionadoControl";
            this.Size = new System.Drawing.Size(969, 176);
            this.Load += new System.EventHandler(this.ComprobanteRelacionadoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).EndInit();
            this.RadCommandBar2.ResumeLayout(false);
            this.RadCommandBar2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxIncluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        internal Telerik.WinControls.UI.CommandBarButton Agregar;
        internal Telerik.WinControls.UI.CommandBarButton Remover;
        internal Telerik.WinControls.UI.CommandBarTextBox ReceptorRFC;
        private Telerik.WinControls.UI.RadCommandBar RadCommandBar2;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarLabel CommandBarLabel1;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.RadCheckBox checkBoxIncluir;
        protected internal Telerik.WinControls.UI.CommandBarHostItem ToolBarHostItemIncluir;
        protected internal Telerik.WinControls.UI.CommandBarDropDownList TipoRelacion;
        protected internal Telerik.WinControls.UI.RadGridView Comprobantes;
    }
}
