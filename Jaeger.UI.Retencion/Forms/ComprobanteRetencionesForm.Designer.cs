﻿namespace Jaeger.UI.Retencion.Forms {
    partial class ComprobanteRetencionesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteRetencionesForm));
            this.TRetencion = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            this.Iconos = new System.Windows.Forms.ImageList(this.components);
            this.menuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TRetencion
            // 
            this.TRetencion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRetencion.Location = new System.Drawing.Point(0, 0);
            this.TRetencion.Name = "TRetencion";
            this.TRetencion.ShowActualizar = true;
            this.TRetencion.ShowAutosuma = false;
            this.TRetencion.ShowCancelar = true;
            this.TRetencion.ShowCerrar = true;
            this.TRetencion.ShowEditar = true;
            this.TRetencion.ShowEjercicio = true;
            this.TRetencion.ShowExportarExcel = false;
            this.TRetencion.ShowFiltro = true;
            this.TRetencion.ShowHerramientas = false;
            this.TRetencion.ShowImprimir = false;
            this.TRetencion.ShowNuevo = true;
            this.TRetencion.ShowPeriodo = true;
            this.TRetencion.Size = new System.Drawing.Size(1291, 30);
            this.TRetencion.TabIndex = 0;
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Version";
            gridViewTextBoxColumn2.HeaderText = "Versión";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Version";
            gridViewTextBoxColumn3.FieldName = "Status";
            gridViewTextBoxColumn3.HeaderText = "Status";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Status";
            gridViewTextBoxColumn3.VisibleInColumnChooser = false;
            gridViewTextBoxColumn4.FieldName = "Folio";
            gridViewTextBoxColumn4.HeaderText = "Folio";
            gridViewTextBoxColumn4.Name = "Folio";
            gridViewTextBoxColumn5.FieldName = "EmisorRFC";
            gridViewTextBoxColumn5.HeaderText = "RFC Emisor";
            gridViewTextBoxColumn5.Name = "EmisorRFC";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "EmisorNombre";
            gridViewTextBoxColumn6.HeaderText = "Nombre del Emisor";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "EmisorNombre";
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn7.HeaderText = "RFC Receptor";
            gridViewTextBoxColumn7.Name = "ReceptorRFC";
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn8.HeaderText = "Nombre del Receptor";
            gridViewTextBoxColumn8.Name = "ReceptorNombre";
            gridViewTextBoxColumn8.Width = 200;
            gridViewTextBoxColumn9.FieldName = "IdDocumento";
            gridViewTextBoxColumn9.HeaderText = "Folio Fiscal (uuid)";
            gridViewTextBoxColumn9.Name = "IdDocumento";
            gridViewTextBoxColumn9.Width = 225;
            gridViewTextBoxColumn10.FieldName = "FechaEmision";
            gridViewTextBoxColumn10.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn10.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn10.Name = "FechaEmision";
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.FieldName = "FechaTimbre";
            gridViewTextBoxColumn11.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn11.HeaderText = "Fec. Timbre";
            gridViewTextBoxColumn11.Name = "FechaTimbre";
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.FieldName = "FechaEstado";
            gridViewTextBoxColumn12.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn12.HeaderText = "Fec. Estado";
            gridViewTextBoxColumn12.Name = "FechaEstado";
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.FieldName = "ClaveRetencion";
            gridViewTextBoxColumn13.HeaderText = "Cve. Retencion";
            gridViewTextBoxColumn13.Name = "ClaveRetencion";
            gridViewTextBoxColumn14.FieldName = "RFCProvCertif";
            gridViewTextBoxColumn14.HeaderText = "RFCProvCertif";
            gridViewTextBoxColumn14.Name = "RFCProvCertif";
            gridViewTextBoxColumn14.Width = 75;
            gridViewComboBoxColumn1.FieldName = "MesInicial";
            gridViewComboBoxColumn1.HeaderText = "MesInicial";
            gridViewComboBoxColumn1.Name = "MesInicial";
            gridViewComboBoxColumn1.Width = 65;
            gridViewComboBoxColumn2.FieldName = "MesFinal";
            gridViewComboBoxColumn2.HeaderText = "MesFinal";
            gridViewComboBoxColumn2.Name = "MesFinal";
            gridViewComboBoxColumn2.Width = 65;
            gridViewTextBoxColumn15.FieldName = "Ejercicio";
            gridViewTextBoxColumn15.HeaderText = "Ejercicio";
            gridViewTextBoxColumn15.Name = "Ejercicio";
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "MontoTotalOperacion";
            gridViewTextBoxColumn16.FormatString = "{0:N2}";
            gridViewTextBoxColumn16.HeaderText = "MontoTotalOperacion";
            gridViewTextBoxColumn16.Name = "MontoTotalOperacion";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 75;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "MontoTotalGravado";
            gridViewTextBoxColumn17.FormatString = "{0:N2}";
            gridViewTextBoxColumn17.HeaderText = "MontoTotalGravado";
            gridViewTextBoxColumn17.Name = "MontoTotalGravado";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 75;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.FieldName = "MontoTotalExento";
            gridViewTextBoxColumn18.FormatString = "{0:N2}";
            gridViewTextBoxColumn18.HeaderText = "MontoTotalExento";
            gridViewTextBoxColumn18.Name = "MontoTotalExento";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 75;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.FieldName = "MontoTotalRetencion";
            gridViewTextBoxColumn19.FormatString = "{0:N2}";
            gridViewTextBoxColumn19.HeaderText = "MontoTotalRetencion";
            gridViewTextBoxColumn19.Name = "MontoTotalRetencion";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 75;
            gridViewTextBoxColumn20.FieldName = "FileXML";
            gridViewTextBoxColumn20.HeaderText = "XML";
            gridViewTextBoxColumn20.Name = "FileXML";
            gridViewTextBoxColumn21.FieldName = "FilePDF";
            gridViewTextBoxColumn21.HeaderText = "PDF";
            gridViewTextBoxColumn21.Name = "FilePDF";
            gridViewTextBoxColumn22.FieldName = "Estado";
            gridViewTextBoxColumn22.HeaderText = "Estado";
            gridViewTextBoxColumn22.Name = "Estado";
            gridViewTextBoxColumn22.Width = 65;
            gridViewTextBoxColumn23.FieldName = "Creo";
            gridViewTextBoxColumn23.HeaderText = "Creo";
            gridViewTextBoxColumn23.Name = "Creo";
            gridViewTextBoxColumn23.Width = 65;
            this.gridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23});
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(1291, 537);
            this.gridData.TabIndex = 2;
            this.gridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.gridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.gridData.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            // 
            // Iconos
            // 
            this.Iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Iconos.ImageStream")));
            this.Iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.Iconos.Images.SetKeyName(0, "FileXML");
            this.Iconos.Images.SetKeyName(1, "FilePDF");
            // 
            // ComprobanteRetencionesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1291, 567);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.TRetencion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComprobanteRetencionesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Retenciones e Información de Pagos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RetencionesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarCommonControl TRetencion;
        private Telerik.WinControls.UI.RadGridView gridData;
        private System.Windows.Forms.ImageList Iconos;
        protected internal Telerik.WinControls.UI.RadContextMenu menuContextual;
    }
}