﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Retencion.Entities;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Retencion.Forms {
    public partial class ComprobanteRetencionForm : RadForm {
        protected internal RadMenuItem DesdeArchivo = new RadMenuItem { Text = "Desde archivo" };
        protected internal RadMenuItem ComplementoPagoAExtranjero = new RadMenuItem { Text = "Pago A Extranjero" };
        protected internal RadMenuItem ComplementoDividendos = new RadMenuItem { Text = "Dividendos" };
        protected CFDISubTipoEnum subTipo;
        protected internal ComplementoPagoAExtranjeroControl pagoAExtranjeroControl = null;

        public ComprobanteRetencionForm(ComprobanteRetencionDetailModel model, CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this.subTipo = subTipo;
            if (model != null) {
                this.ComprobanteControl.Comprobante = model;
            }
        }

        private void Comprobante1RetencionForm_Load(object sender, EventArgs e) {
            this.ComprobanteControl.Start(this.subTipo);
            this.ComprobanteControl.BindingCompleted += ComprobanteControl_BindingCompleted;
            this.ComprobanteControl.TRetencion.Complementos.Items.Add(this.DesdeArchivo);
            this.ComprobanteControl.TRetencion.Complementos.Items.Add(this.ComplementoPagoAExtranjero);
            this.ComprobanteControl.TRetencion.Complementos.Items.Add(this.ComplementoDividendos);
            this.DesdeArchivo.Click += this.DesdeArchivo_Click;
            this.ComplementoPagoAExtranjero.Click += ComplementoPagoAExtranjero_Click;
            this.ComprobanteControl.TRetencion.Cerrar.Click += this.Cerrar_Click;
        }

          #region barra de heramientas
        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void DesdeArchivo_Click(object sender, EventArgs e) {
            var _openFile = new OpenFileDialog();
            if (_openFile.ShowDialog() == DialogResult.OK) {
                this.ComprobanteControl.Comprobante = this.ComprobanteControl.Service.GetComprobante(_openFile.FileName);
                this.ComprobanteControl.TRetencion.Actualizar.PerformClick();
            }
        }

        private void ComprobanteControl_BindingCompleted(object sender, EventArgs e) {
            if (ComprobanteControl.Comprobante.Pagosaextranjeros != null) {
                this.ComplementoPagoAExtranjero_Click(sender, null);
            }
        }

        private void ComplementoPagoAExtranjero_Click(object sender, EventArgs e) {
            if (this.pagoAExtranjeroControl == null) {
                var _page = new RadPageViewPage() { Text = "Complemento: Pago A Extranjero", Padding = new Padding(3), AutoScroll = true };
                this.pagoAExtranjeroControl = new ComplementoPagoAExtranjeroControl() { Dock = DockStyle.Fill };
                this.pagoAExtranjeroControl.Start();
                _page.Controls.Add(this.pagoAExtranjeroControl);
                this.TabControl.Controls.Add(_page);

                if (this.ComprobanteControl.Comprobante.Pagosaextranjeros == null) {
                    this.ComprobanteControl.Comprobante.Pagosaextranjeros = new Domain.Retencion.Entities.Complemento.ComplementoPagosaextranjeros();
                }
                this.pagoAExtranjeroControl.Complemento = this.ComprobanteControl.Comprobante.Pagosaextranjeros;
                this.pagoAExtranjeroControl.CreateBinding();
                this.pagoAExtranjeroControl.Editable = this.ComprobanteControl.Comprobante.Editable;
            } else {
                this.pagoAExtranjeroControl.Complemento = this.ComprobanteControl.Comprobante.Pagosaextranjeros;
                this.pagoAExtranjeroControl.CreateBinding();
                this.pagoAExtranjeroControl.Editable = this.ComprobanteControl.Comprobante.Editable;
            }
        }
        #endregion
    }
}
