﻿namespace Jaeger.UI.Retencion.Forms {
    partial class ComprobanteRetencionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.ComprobanteControl = new Jaeger.UI.Retencion.Forms.ComprobanteFiscalControl();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 364);
            this.TabControl.Name = "TabControl";
            this.TabControl.Size = new System.Drawing.Size(1178, 86);
            this.TabControl.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // ComprobanteControl
            // 
            this.ComprobanteControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ComprobanteControl.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteControl.Name = "ComprobanteControl";
            this.ComprobanteControl.Size = new System.Drawing.Size(1178, 364);
            this.ComprobanteControl.TabIndex = 0;
            // 
            // ComprobanteRetencionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 450);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.ComprobanteControl);
            this.Name = "ComprobanteRetencionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Retención";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Comprobante1RetencionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal ComprobanteFiscalControl ComprobanteControl;
        protected internal Telerik.WinControls.UI.RadPageView TabControl;
    }
}